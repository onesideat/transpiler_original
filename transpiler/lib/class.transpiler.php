<?php

use \ScssPhp\ScssPhp;
use \JShrink\Minifier;

class Transpiler {
    /**
     * Options
     *
     * @var array $options
     */
    private $options = [
        // mode of using: 'test', 'live'
        'mode' => 'test',

        // paths
        'paths' => [
            'css' => './css/',
            'scss' => './scss/',
            'js' => './js/',
            'sjs' => './sjs/',
        ],

        // nodejs absolute path
        'node' => null,

        // use javascript transpiler
        'js_transpiler' => true
    ];

    /**
     * __construct
     *
     * Initialize the class.
     * Merge the options and compile source depending on mode
     *
     * @access private
     * @return void
     */
    public function __construct($options = array()) {
        $this->options = array_merge($this->options, $options);

        // make paths real
        foreach ($this->options['paths'] as $key => $val) {
            if (is_dir($val))
                $this->options['paths'][$key] = realpath($val) . '\\';
        }

        // find nodejs path
        if (is_null($this->options['node'])) {
            $env = explode(';', getenv('PATH'));
            foreach($env as $e) {
                if (strpos($e, 'nodejs') > -1) {
                    $this->options['node'] = $e . 'node';
                }
            }
        }

        // check dependencies
        if (!class_exists('\ScssPhp\ScssPhp\Compiler') || !class_exists('\JShrink\Minifier'))
            $this->throwError('No ScssPhp or JShrink dependency in PHP founded! Call <code>composer install</code>.', realpath(__DIR__ . '/../'), true);
        if (is_string($this->options['node']) && !file_exists($this->options['node']) && !file_exists($this->options['node'] . '.exe'))
            $this->throwError('Wrong <code>Node.js</code> path.', realpath(__DIR__ . '/../'), true);
        if ($this->options['mode'] == 'live' && $this->options['js_transpiler'] && (!is_string($this->options['node']) || !is_dir(__DIR__ . '/../node_modules/')))
            $this->throwError('Node.js with node_modules are required while transpiling', realpath(__DIR__ . '/../'), true);

        // processRequest
        if (isset($_SERVER['REQUEST_URI']))
            $this->processRequest();
        if ($this->isCLI())
            $this->processCli();
    }

    /**
     * processRequest
     *
     * Process the request done from .htaccess
     *
     * @access private
     * @return void
     */
    private function processRequest() {
        $requestURI = substr(strtok($_SERVER['REQUEST_URI'], '?'), 1);
        $minifyCode = (isset($_GET['minify']) && $_GET['minify'] == 'true') || $this->options['mode'] == 'live';
        $requestFile = str_replace('/', '\\', $requestURI);
        $relative_file = PUBLIC_PATH . '\\' . $requestFile;
        $pathinfo = pathinfo($relative_file);

        if (!in_array($pathinfo['extension'], ['js', 'css']))
            return;

        switch($pathinfo['extension']) {
            case 'css':
                $scssFile = str_replace([$this->options['paths']['css'], '.css'], [$this->options['paths']['scss'], '.scss'], $relative_file);

                header("HTTP/1.1 200 OK");
                header("Content-type: text/css", true);
                if (is_file($scssFile)) {
                    echo $this->compileScss($scssFile, dirname(realpath($scssFile)), $minifyCode);
                    exit;
                } else if (is_file($relative_file)) {
                    readfile($relative_file);
                    exit;
                }

                break;
            case 'js':
                $sjsFile = str_replace([$this->options['paths']['js']], [$this->options['paths']['sjs']], $relative_file);

                header("HTTP/1.1 200 OK");
                header("Content-type: application/javascript", true);
                if (is_file($sjsFile)) {
                    echo $this->compileJs($sjsFile, $minifyCode);
                    exit;
                } else if (is_file($relative_file)) {
                    readfile($relative_file);
                    exit;
                }

                break;
        }

        header("HTTP/1.0 404 Not Found");
        die();
    }

    /**
     * processCli
     *
     * Compile the source files and create an exact file tree copy
     *
     * @access private
     * @return void
     */
    private function processCli() {
        global $argv;

        $output = [
            'css_count' => 0,
            'js_count' => 0,
            'warn_css_count' => 0,
            'warn_js_count' => 0,
            'time' => microtime(true)
        ];
        array_shift($argv);

        if (!is_dir($this->options['paths']['css']))
            mkdir($this->options['paths']['css'], 0777);
        if (!is_dir($this->options['paths']['js']))
            mkdir($this->options['paths']['js'], 0777);

        if (count($argv) > 0) {
            foreach($argv as $key => $file) {
                if (!is_file($file) || !in_array(pathinfo($file, PATHINFO_EXTENSION), ['scss', 'js']))
                    continue;

                $type = (str_replace('scss', 'css', pathinfo($file, PATHINFO_EXTENSION)));

                $dist = $this->generateDistFile($file);
                if ($dist)
                    $output[$type.'_count']++;
                else
                    $output['warn_'.$type.'_count']++;
            }
        } else {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->options['paths']['scss']), RecursiveIteratorIterator::LEAVES_ONLY);
            foreach($files as $name => $file) {
            	if (!$file->isDir()) {
                    if ($file->getFileName()[0] == '_' || pathinfo($file->getFileName(), PATHINFO_EXTENSION) != 'scss')
                        continue;

                    $dist = $this->generateDistFile($file->getRealPath());
                    if ($dist)
                        $output['css_count']++;
                    else
                        $output['warn_css_count']++;
            	}
            }

            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->options['paths']['sjs']), RecursiveIteratorIterator::LEAVES_ONLY);
            foreach($files as $name => $file) {
            	if (!$file->isDir()) {
                    if (strpos($file->getRealPath(), 'portal_old') !== false)
                        continue;
                    if ($file->getFileName()[0] == '_' || pathinfo($file->getFileName(), PATHINFO_EXTENSION) != 'js')
                        continue;

                    $dist = $this->generateDistFile($file->getRealPath());
                    if ($dist)
                        $output['js_count']++;
                    else
                        $output['warn_js_count']++;
            	}
            }
        }

        // console output
        $output['time'] = microtime(true) - $output['time'];
        echo "\n---------------------------------------------\n";
        if ($output['css_count'] > 0 || $output['js_count'] > 0) {
            echo "\e[92mSuccess! \e[39m\n";
            echo "CSS (". $output['css_count'] .") and JavaScript (". $output['js_count'] .") files created in " . round($output['time'], 3) . "s";
        }
        if ($output['warn_css_count'] > 0 || $output['warn_js_count'] > 0) {
            echo "\n\e[91mFailed files! Fix the above issues and try again. \e[39m\n";
            echo "CSS (". $output['warn_css_count'] .") and JavaScript (". $output['warn_js_count'] .") files";
        }
        echo "\n";
    }

    /**
     * generateDistFile
     *
     * Create a compiled and minified dist file from the source
     *
     * @param string $file - a js or scss source file path
     *
     * @access private
     * @return boolean status
     */
    private function generateDistFile($file) {
        $pathinfo = pathinfo($file);
        $type = str_replace('scss', 'css', $pathinfo['extension']);
        $distPath = $this->options['paths'][$type] . substr($pathinfo['dirname'], strlen($this->options['paths']['s'.$type]));
        $distFile = $distPath . '/' . $pathinfo['filename'] . '.' . $type;
        $content = '';

        if (!is_dir($distPath))
            mkdir($distPath, 0777, true);

        if ($type == 'css')
            $content = $this->compileScss($file, dirname($file), true);
        if ($type == 'js')
            $content = $this->compileJs($file, true);

        if ($content === false)
            return false;

        $fp = fopen($distFile, "w");
        fwrite($fp, $content);
        fclose($fp);

        return true;
    }

    /**
     * compileScss
     *
     * Compile the scss code to css
     *
     * @param string $file - path to scss file
     * @param string $scssPath - scss imports, used to follow the file tree
     * @param boolean $minify - minify the code
     *
     * @access private
     * @return string $content
     */
    private function compileScss($file, $scssPath = './', $minify = false) {
        try {
            $scss = new ScssPhp\Compiler();
            $scss->setImportPaths($scssPath);
            $scss->setFormatter('\ScssPhp\ScssPhp\Formatter\\' . ($minify ? 'Crunched' : 'Expanded'));
            $scss->setLineNumberStyle(ScssPhp\Compiler::LINE_COMMENTS);
            $content = file_get_contents($file);
            $header = '';

            if (in_array(substr($content, 0, 2), ['//', '/*']) && preg_match('/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/m', $content, $preg))
                $header = $preg[0];

            $content = (!empty($header) ? $header . PHP_EOL : '') . $scss->compile($content);

            return $content;
        } catch (\Exception $e) {
            $this->throwError($e->getMessage(), $file);

            return false;
        }
    }

    /**
     * compileJs
     *
     * Compile the sjs code to js
     *
     * @param string $file - path to js file
     * @param boolean $minify - minify the code
     *
     * @access private
     * @return string $content
     */
    private function compileJs($file, $minify = false) {
        try {
            if (!$minify) {
                $content = file_get_contents($file);
                $header = '';

                if (in_array(substr($content, 0, 2), ['//', '/*']) && preg_match('/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/m', $content, $preg))
                    $header = $preg[0];

                $content = (!empty($header) ? $header . PHP_EOL : '') . $this->jsCodeWithImports($content, $file);
            } else if ($this->options['js_transpiler']) {
                $content = shell_exec('"'. $this->options['node'] .'"' . ' "'. (realpath(__DIR__ . '/../')) .'/jscompiler.js" "'. $file .'" --compress 2>&1');
                $content = trim($content);

                if (strpos($content, 'm: Expected ') !== false) {
                    if ($this->isCLI()) {
                        echo $content;
                        $this->throwError('', $file);
                    } else {
                        $this->throwError('Syntax error in JavaScript', $file);
                    }

                    return false;
                }
            } else {
                $content = file_get_contents($file);
                $content = \JShrink\Minifier::minify($this->jsCodeWithImports($content, $file), array('flaggedComments' => false));
            }

            return $content;
        } catch (\Exception $e) {
            $this->throwError($e->getMessage(), $file);

            return false;
        }
    }

    /**
     * jsCodeWithImports
     *
     * Search for imports and include them into js file
     * Syntax: import 'path/to/file';
     *         import 'path/to/file.js';
     *
     * @param string $code - javascript code
     * @param string $file - javascript file, used to follow the file tree
     *
     * @access private
     * @return string $code
     */
    private function jsCodeWithImports($code, $file) {
        preg_match_all('/import [\'"](.+?)[\'"](?:;|)/i', $code, $matches);

        if (count($matches[0]) > 0) {
            foreach($matches[0] as $key => $match) {
                $nc = '';
                $pathinfo = pathinfo($file);
                $pathinfo2 = pathinfo($matches[1][$key]);
                $newFile = $pathinfo['dirname'] . '/' . $pathinfo2['dirname'] . '/_' . $pathinfo2['filename'] . '.js';
                preg_match('/^.*' . preg_quote($match, '/') . '.*$/im', $code, $line);

                if (substr(trim($line[0]), 0, 2) == '//' || (substr(trim($line[0]), 0, 2) == '/*' && substr(trim($line[0]), -2) == '*/')) {
                    $code = str_replace($line[0], '', $code);
                    continue;
                }

                if (file_exists($newFile)) {
                    $nc = file_get_contents($newFile);
                    $nc = $this->jsCodeWithImports($nc, $newFile);
                }

                $code = str_replace($match, $nc, $code);
            }
        }

        return $code;
    }

    /**
     * isCLI
     *
     * Find out if code is exectued in command line
     *
     * @access private
     * @return boolean
     */
    private function isCLI() {
        return php_sapi_name() == 'cli';
    }

    /**
     * throwError
     *
     * Show an error to developer
     *
     * @param string $message - Error message
     * @param string $file - Place where the error was triggered
     * @param boolean $critical - Stop the execution if critical
     *
     * @access private
     * @return void exit
     */
    private function throwError($message, $file, $critical = false) {
        if (!$this->isCLI()) {
            header("HTTP/1.1 500 Internal Server Error");
            header("Content-Type: text/html");

            echo '<!doctype html><html lang="en"><head><meta charset="utf-8"><title>'.$file.'</title></head><body>';
            echo '<p>Transpiler error: in <code>'. $file .'</code></p><pre>'. $message .'</pre>';
            echo '</body></html>';

            exit;
        } else {
            echo "\n\e[91mError! \e[39m\n";
            echo "Transpiler error in \e[93m" . $file . "\e[39m\n";
            echo $message . "\n";

            if ($critical)
                exit();
        }
    }
}