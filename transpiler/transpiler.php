<?php

require 'vendor/autoload.php';
require 'lib/class.transpiler.php';

define('PUBLIC_PATH', realpath(__DIR__ . '/../'));

new Transpiler([
    'mode' => isset($_SERVER['REQUEST_URI']) ? 'test' : 'live',

    'paths' => [
        'css' => PUBLIC_PATH . '/assets/css/',
        'scss' => PUBLIC_PATH . '/assets/scss/',
        'js' => PUBLIC_PATH . '/assets/js/',
        'sjs' => PUBLIC_PATH . '/assets/sjs/',
    ],

    'js_transpiler' => false
 ]);