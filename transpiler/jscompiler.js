const path = require('path');
const fs = require("fs");
const swc = require("@swc/core");
const prettier = require("prettier");
const uglifyJS = require("uglify-js");

let argv = process.argv.slice(2);
let file = argv[0];
let output = "";

// read file
if (!fs.existsSync(file)) {
    console.error('File not found!');
    process.exit();
}
output = fs.readFileSync(file, 'utf8');
output = codeWithImports(output, file);

// transpiler
if (argv.indexOf('--no-transpiler') == -1) {
    try {
        output = swc.transformSync(output, {
            jsc: {
                target: "es2015"
            }
        }).code;
    } catch (e) {
        process.exit();
    }
}

// prettier
if (argv.indexOf('--no-prettier') == -1) {
    try {
        output = prettier.format(output, {
            trailingComma: 'es5',
            tabWidth: 4,
            semi: true,
            singleQuote: true,
            arrowParens: 'always',
            bracketSpacing: true,
            parser: 'babel'
        });
    } catch (e) {
        console.error(e.message);
        process.exit();
    }
}

// uglify
if (argv.indexOf('--compress') > -1) {
    try {
        output = uglifyJS.minify(output, {
            mangle: true
        }).code;
    } catch (e) {
        console.error(e.message);
        process.exit();
    }
}

// output
console.log(output);

/* private functions */
function codeWithImports(code, file) {
    let re = /import ['"](.+?)['"](?:;|)/gmi;

    for (const match of code.matchAll(re)) {
        let nc = '',
            line = code.match(new RegExp('^.*' + match[0].replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '.*$', 'm')),
            newFile = path.dirname(file) + '/' + path.dirname(match[1]) + '/_' + path.basename(match[1], '.js') + '.js';

        if (line[0].trim().substr(0, 2) == '//' || (line[0].trim().substr(0, 2) == '/*' && line[0].trim().substr(line[0].length, 2) == '*/')) {
            code = code.replace(line[0], '');
            continue;
        }

        if (fs.existsSync(newFile)) {
            nc = fs.readFileSync(newFile, 'utf8') + ';';
            nc = codeWithImports(nc, newFile);
        }

        code = code.replace(match[0], nc);
    }

    return code;
}